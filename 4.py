inp = '4-input'
pairs = []

s = 0
o = 0
for line in open(inp):
    pair = line.strip().split(',')
    c1 = [int(x) for x in pair[0].split('-')]
    c2 = [int(x) for x in pair[1].split('-')]

    if (c1[0] <= c2[0] and c1[1] >= c2[1]) or \
            (c2[0] <= c1[0] and (c2[1] >= c1[1])):
        s += 1

    if c1 > c2:
        c1, c2 = c2, c1
    if (c1[1] >= c2[0]):
        o += 1
        print('NOT')
    else:
        print("...", c1, c2)

print(s)
print(o)
