scorelines = """
R 1
P 2
S 3
R R 4
P P 5
S S 6
R P 8
R S 3
P R 1
P S 9
S R 7
S P 2
"""
scores = {}
for s in scorelines.strip().splitlines():
    k, v = s.rsplit(maxsplit=1)
    scores[k] = int(v)
print(scores)

c = 0
c2 = 0
tr = str.maketrans('ABCXYZ', 'RPSRPS')
wr = str.maketrans('RPS', 'PSR')
lr = str.maketrans('RPS', 'SRP')
for line in open('2-input'):
    d = line.strip().translate(tr)
    c += scores[d]

    # part 2
    """X - lose, Y - draw, Z - win"""
    s = d
    if line.strip()[-1] == 'X':
        s = s[0] + ' ' + s[0].translate(lr)
    if line.strip()[-1] == 'Y':
        s = s[0] + ' ' + s[0]
        # print(line.strip(), d, s)
    if line.strip()[-1] == 'Z':
        s = s[0] + ' ' + s[0].translate(wr)
    c2 += scores[s]

print(c)
print(c2)
