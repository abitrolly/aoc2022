elves = []

s = 0
for line in open('1-input'):
    if not line.strip():
        if s:
            elves.append(s)
        s = 0
    else:
        s += int(line.strip())

print(max(elves))
print(sum(sorted(elves)[-3:]))
