import string
from collections import OrderedDict

letters = string.ascii_lowercase + string.ascii_uppercase
numbers = range(1, 53)  # range last element is not inclusive
priority = OrderedDict(
  (letters[i], numbers[i]) for i in range(len(letters))
)

s = 0
group = []
g = 0
for line in open('3-input').read().splitlines():
    size = len(line) // 2
    c1, c2 = line[:size], line[size:]
    both = set(c1) & set(c2)
    s += priority[both.pop()]

    if len(group) < 3:
        group.append(set(line))
        if len(group) == 3:
            badge = group[0] & group[1] & group[2]
            g += priority[badge.pop()]
            group = []

print(s)
print(g)
